#include "pin.H"
extern "C" {
#include "xed-interface.h"
}
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <errno.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <values.h>
#include <algorithm>
#include <map>

#define PROFILE_FILE_NAME "__profile.map"
#define OPT_RTN_NUM 10
#define OPT_FACTOR 2
ofstream outFile;

UINT64 rtnIndex = 0;

KNOB<BOOL> ProfKnob(KNOB_MODE_WRITEONCE, "pintool",
    "prof", "0", "Ex2 Edge Profiling");
KNOB<BOOL> Opt1Knob(KNOB_MODE_WRITEONCE, "pintool",
    "opt0", "0", "Ex3 Probe Mode");
KNOB<BOOL> Opt2Knob(KNOB_MODE_WRITEONCE, "pintool",
    "opt1", "0", "Ex4 Probe Mode");
KNOB<BOOL> Opt3Knob(KNOB_MODE_WRITEONCE, "pintool",
    "opt2", "0", "Project Probe Mode");
KNOB<BOOL>   KnobVerbose(KNOB_MODE_WRITEONCE, "pintool",
    "verbose", "0", "Verbose run");
KNOB<BOOL>   KnobDumpTranslatedCode(KNOB_MODE_WRITEONCE, "pintool",
    "dump_tc", "0", "Dump Translated Code");
KNOB<BOOL>   KnobDoNotCommit(KNOB_MODE_WRITEONCE, "pintool",
    "no-commit", "0", "Do not commit translated code");
KNOB<BOOL>   KnobDebug(KNOB_MODE_WRITEONCE, "pintool",
    "debug1", "0", "Debug messages phase1");
KNOB<BOOL>   KnobDebug2(KNOB_MODE_WRITEONCE, "pintool",
    "debug2", "0", "Debug messages phase2");
KNOB<BOOL>   KnobDebug3(KNOB_MODE_WRITEONCE, "pintool",
    "debug3", "0", "Debug messages phase3");
KNOB<INT>    KnobOptMethod(KNOB_MODE_WRITEONCE, "pintool",
	"opt-method", "0", "Optimization method (0/1/2)");
KNOB<UINT64>    KnobInlineSize(KNOB_MODE_WRITEONCE, "pintool",
	"inline-size", "2000", "Instrucions thershold for inline");

/* ========================= */
/*            Ex2            */
/* ========================= */
ADDRINT mainExeImageLowAddr = 0;
ADDRINT mainExeImageHighAddr = 0;

typedef struct BblInfo
{
  ADDRINT start;
  ADDRINT end;
  ADDRINT fallThroughAddr;
  UINT64 fallThroughCount;
  ADDRINT takenAddr;
  UINT64 takenCount;
  bool isCall;
} BBLINFO;

typedef struct BblEdgeInfo
{
  UINT64 maxEdgeIndex;
  UINT64 edgeCount;
} BBLEDGEINFO;
  
typedef map<ADDRINT, BBLINFO*> BBLLIST;

typedef struct RtnInfo
{
  string name;
  UINT64 icount;
  ADDRINT addr;
  BBLLIST bblList;
} RTNINFO;

typedef map<ADDRINT, RTNINFO*> RTNLIST;
RTNLIST rtnList;

typedef map<ADDRINT, UINT64> INLINEDINDEXLIST;
INLINEDINDEXLIST inlinedIndexList;

int compareFunc(RTNINFO* in1, RTNINFO* in2){
  return in1->icount > in2->icount;
}

VOID saveProfile ()
{
  ofstream profileFile;
  profileFile.open(PROFILE_FILE_NAME);

  profileFile << rtnList.size() << endl;
  for (RTNLIST::iterator rtnIt = rtnList.begin(); rtnIt != rtnList.end(); ++rtnIt)
  {
    profileFile << rtnIt->first << endl;; //offset from image start
    profileFile << rtnIt->second->name << endl;
    profileFile << rtnIt->second->icount << endl;

    profileFile << rtnIt->second->bblList.size() << endl;
    for (BBLLIST::iterator bblIt = rtnIt->second->bblList.begin(); bblIt != rtnIt->second->bblList.end(); ++bblIt)
    {
      profileFile << bblIt->first << endl; //offset from image start
      profileFile << (bblIt->second->end - mainExeImageLowAddr) << endl;
      profileFile << (bblIt->second->takenAddr - mainExeImageLowAddr) << endl;
      profileFile << (bblIt->second->fallThroughAddr - mainExeImageLowAddr) << endl;
      profileFile << bblIt->second->fallThroughCount << endl;
      profileFile << bblIt->second->takenCount << endl;
      profileFile << boolalpha << bblIt->second->isCall << noboolalpha << endl;
    }

  }
  profileFile.close();
}

VOID updateProfile()
{
  ifstream profileFile;
  profileFile.open(PROFILE_FILE_NAME);

  size_t numRtn;
  ADDRINT rtnOffset;
  RTNINFO* rtnInfoPtr;

  size_t numBbl;
  ADDRINT bblOffset;
  ADDRINT bblTailOffset;
  ADDRINT bblTakenOffset;
  ADDRINT bblFallThroughOffset;
  BBLINFO* bblInfoPtr;
  string isCallString;

  profileFile >> numRtn;

  for (size_t i = 0; i < numRtn; ++i)
  {
    rtnInfoPtr = new RTNINFO;
    profileFile >> rtnOffset;
    rtnInfoPtr->addr = rtnOffset + mainExeImageLowAddr;
    profileFile >> rtnInfoPtr->name;
    profileFile >> rtnInfoPtr->icount;

    rtnList[rtnOffset] = rtnInfoPtr;

    profileFile >> numBbl;
    for (size_t j = 0; j < numBbl; ++j)
    {
      bblInfoPtr = new BBLINFO;
      profileFile >> bblOffset;
      profileFile >> bblTailOffset;
      profileFile >> bblTakenOffset;
      profileFile >> bblFallThroughOffset;
      bblInfoPtr->start = bblOffset + mainExeImageLowAddr;
      bblInfoPtr->end = bblTailOffset + mainExeImageLowAddr;
      bblInfoPtr->takenAddr = bblTakenOffset + mainExeImageLowAddr;
      bblInfoPtr->fallThroughAddr = bblFallThroughOffset + mainExeImageLowAddr;
      profileFile >> bblInfoPtr->fallThroughCount;
      profileFile >> bblInfoPtr->takenCount;
      profileFile >> isCallString;
      bblInfoPtr->isCall = (isCallString == "true" ? true : false);

      rtnInfoPtr->bblList[bblOffset] = bblInfoPtr;
    }

  }
  profileFile.close();

}

// This function is called before every instruction is executed
VOID inscount(UINT32 bblIns, UINT64* icount)
{
  (*icount) += bblIns;
}

VOID edgecount(BOOL isTaken, BBLINFO* bblInfoPtr)
{
  if(isTaken)
  {
    bblInfoPtr->takenCount++;
  }
  else
  {
    bblInfoPtr->fallThroughCount++;
  }
}

VOID Trace(TRACE trace, VOID *v)
{
  for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl))
  { 
    INS head = BBL_InsHead(bbl);
    ADDRINT headAddr = INS_Address(head);
    INS tail = BBL_InsTail(bbl);
    ADDRINT tailAddr = INS_Address(tail);

    if((headAddr < mainExeImageLowAddr) || (tailAddr > mainExeImageHighAddr))
      continue;

    RTN rtn = RTN_FindByAddress(headAddr);
    if (RTN_Valid(rtn))
    {
      string rtnName = RTN_Name(rtn);
      ADDRINT rtnOffset = RTN_Address(rtn) - mainExeImageLowAddr;
      RTNINFO* rtnInfoPtr;

      if (rtnList.count(rtnOffset) > 0)
      {
        rtnInfoPtr = rtnList[rtnOffset];
      }
      else
      {
        rtnInfoPtr = new RTNINFO;
        rtnInfoPtr->icount = 0;
        rtnInfoPtr->name = RTN_Name(rtn);
        rtnInfoPtr->addr = RTN_Address(rtn);

        rtnList[rtnOffset] = rtnInfoPtr;
      }

      BBL_InsertCall(bbl, IPOINT_BEFORE, (AFUNPTR)inscount, IARG_UINT32, 
          BBL_NumIns(bbl), IARG_PTR, &(rtnInfoPtr->icount), IARG_END);


      BBLINFO* bblInfoPtr;
      ADDRINT bblOffset = headAddr - mainExeImageLowAddr;
      if (rtnInfoPtr->bblList.count(bblOffset) > 0)
      {
        bblInfoPtr = rtnInfoPtr->bblList[bblOffset];
      }
      else
      {
        bblInfoPtr = new BBLINFO;
        bblInfoPtr->fallThroughCount = 0;
        bblInfoPtr->takenCount = 0;
        bblInfoPtr->start = headAddr;
        bblInfoPtr->end = tailAddr;

        if (INS_IsDirectBranchOrCall(tail))
        { 
          bblInfoPtr->isCall = INS_IsCall(tail);
          assert(bblInfoPtr->isCall <= 1);
          bblInfoPtr->fallThroughAddr = INS_NextAddress(tail);
          bblInfoPtr->takenAddr = INS_DirectBranchOrCallTargetAddress(tail);
        }

        rtnInfoPtr->bblList[bblOffset] = bblInfoPtr;
      }

      if (INS_IsDirectBranchOrCall(tail))
      {
        INS_InsertCall(tail, IPOINT_BEFORE, (AFUNPTR) edgecount, 
            IARG_BRANCH_TAKEN, IARG_PTR, bblInfoPtr, IARG_END);
      }
    }
  }
}

VOID Image(IMG img, VOID *v) {
  if(IMG_IsMainExecutable(img)) 
  {
    mainExeImageLowAddr = IMG_LowAddress(img);
    mainExeImageHighAddr = IMG_HighAddress(img);    

    ifstream profileFile;
    if(ifstream(PROFILE_FILE_NAME))
    {
      updateProfile();
    }

  }
}

INT64 getIndexByAddress(vector<BBLINFO*> bblInfoVector, ADDRINT addr)
{
  for (UINT64 i = 0; i < bblInfoVector.size(); ++i)
  {
    if (bblInfoVector[i]->start == addr)
    {
      return i;
    }
  }
  return -1;
}

VOID Fini(INT32 code, VOID *v)
{

  saveProfile();
  vector<RTNINFO*> rtnInfoVector;
  for (RTNLIST::iterator rtnIt = rtnList.begin(); rtnIt != rtnList.end(); ++rtnIt)
  {
    rtnInfoVector.push_back(rtnIt->second);
  }

  sort(rtnInfoVector.begin(), rtnInfoVector.end(), compareFunc);

  for (vector<RTNINFO*>::iterator rtnIt = rtnInfoVector.begin(); rtnIt != rtnInfoVector.end(); ++rtnIt)
  {
    outFile << "\"" + (*rtnIt)->name + "\"" << " 0x" << hex << (*rtnIt)->addr << " icount: " << dec << (*rtnIt)->icount << endl;

    vector<BBLINFO*> bblInfoVector;
    for (BBLLIST::iterator bblIt = (*rtnIt)->bblList.begin(); bblIt != (*rtnIt)->bblList.end(); ++bblIt)
    {
      bblInfoVector.push_back(bblIt->second);
    }
    for (UINT64 i = 0; i < bblInfoVector.size(); ++i)
    {   
      outFile << "\t" << "BBL" << dec << i << ": 0x" << hex << bblInfoVector[i]->start << " - 0x" << bblInfoVector[i]->end <<  " CALL " << bblInfoVector[i]->isCall << endl;
    }
    outFile << endl;

    for (UINT64 i = 0; i < bblInfoVector.size()*2; i+=2)
    { 
      INT64 fallThroughIndex = getIndexByAddress(bblInfoVector, bblInfoVector[i/2]->fallThroughAddr); 
      INT64 takenIndex = getIndexByAddress(bblInfoVector, bblInfoVector[i/2]->takenAddr);  
      if ((fallThroughIndex != -1) && (takenIndex != -1))
      {
        outFile << "\t" << "Edge" << dec << i << ": BBL" << i / 2 << " -> BBL" << fallThroughIndex << " " << bblInfoVector[i/2]->fallThroughCount << endl;
        outFile << "\t" << "Edge" << dec << i  + 1 << ": BBL" << i / 2 << " -> BBL" << takenIndex << " " << bblInfoVector[i/2]->takenCount << endl;
      }
    }
    for (UINT64 i = 0; i < bblInfoVector.size()*2; i+=2)
    { 
      delete bblInfoVector[i / 2];
    }
    outFile << endl;
    delete (*rtnIt);
  }   
}


/* ========================= */
/*            Ex3            */
/* ========================= */

std::ofstream* out = 0;

// For XED:
#if defined(TARGET_IA32E)
xed_state_t dstate = {XED_MACHINE_MODE_LONG_64, XED_ADDRESS_WIDTH_64b};
#else
xed_state_t dstate = { XED_MACHINE_MODE_LEGACY_32, XED_ADDRESS_WIDTH_32b};
#endif

//For XED: Pass in the proper length: 15 is the max. But if you do not want to
//cross pages, you can pass less than 15 bytes, of course, the
//instruction might not decode if not enough bytes are provided.
const unsigned int max_inst_len = XED_MAX_INSTRUCTION_BYTES;

ADDRINT lowest_sec_addr = 0;
ADDRINT highest_sec_addr = 0;

#define MAX_PROBE_JUMP_INSTR_BYTES  14

// tc containing the new code:
char *tc; 
UINT64 tc_cursor = 0;

// instruction map with an entry for each new instruction:
typedef struct { 
  ADDRINT orig_ins_addr;
  ADDRINT new_ins_addr;
  ADDRINT orig_targ_addr;
  bool hasNewTargAddr;
  char encoded_ins[XED_MAX_INSTRUCTION_BYTES];
  xed_category_enum_t category_enum;
  unsigned int size;
  int new_targ_entry;
  UINT64 inlinedIndex;
} instr_map_t;


instr_map_t *instr_map = NULL;
int num_of_instr_map_entries = 0;
int max_ins_count = 0;


// total number of routines in the main executable module:
int max_rtn_count = 0;

// Tables of all candidate routines to be translated:
typedef struct { 
  ADDRINT rtn_addr; 
  USIZE rtn_size;
  int instr_map_entry;   // negative instr_map_entry means routine does not have a translation.
  bool isSafeForReplacedProbe;  
} translated_rtn_t;

translated_rtn_t *translated_rtn;
int translated_rtn_num = 0;

void dump_all_image_instrs(IMG img)
{
  for (SEC sec = IMG_SecHead(img); SEC_Valid(sec); sec = SEC_Next(sec))
  {   
    for (RTN rtn = SEC_RtnHead(sec); RTN_Valid(rtn); rtn = RTN_Next(rtn))
    {   

      // Open the RTN.
      RTN_Open( rtn );

      cerr << RTN_Name(rtn) << ":" << endl;

      for( INS ins = RTN_InsHead(rtn); INS_Valid(ins); ins = INS_Next(ins) )
      {       
        cerr << "0x" << hex << INS_Address(ins) << ": " << INS_Disassemble(ins) << endl;
      }

      // Close the RTN.
      RTN_Close( rtn );
    }
  }
}

void dump_instr_from_xedd (xed_decoded_inst_t* xedd, ADDRINT address)
{
  // debug print decoded instr:
  char disasm_buf[2048];

  xed_uint64_t runtime_address = reinterpret_cast<xed_uint64_t>(address);  // set the runtime adddress for disassembly  

  xed_format_context(XED_SYNTAX_INTEL, xedd, disasm_buf, sizeof(disasm_buf), static_cast<UINT64>(runtime_address), 0, 0); 

  cerr << hex << address << ": " << disasm_buf <<  endl;
}

void dump_instr_from_mem (ADDRINT *address, ADDRINT new_addr)
{
  char disasm_buf[2048];
  xed_decoded_inst_t new_xedd;

  xed_decoded_inst_zero_set_mode(&new_xedd,&dstate); 

  xed_error_enum_t xed_code = xed_decode(&new_xedd, reinterpret_cast<UINT8*>(address), max_inst_len);          

  BOOL xed_ok = (xed_code == XED_ERROR_NONE);
  if (!xed_ok){
    cerr << "invalid opcode" << endl;
    return;
  }

  xed_format_context(XED_SYNTAX_INTEL, &new_xedd, disasm_buf, 2048, static_cast<UINT64>(new_addr), 0, 0);

  cerr << "0x" << hex << new_addr << ": " << disasm_buf <<  endl;  

}

void dump_entire_instr_map()
{ 
  for (int i=0; i < num_of_instr_map_entries; i++) {
    for (int j=0; j < translated_rtn_num; j++) {
      if (translated_rtn[j].instr_map_entry == i) {

        RTN rtn = RTN_FindByAddress(translated_rtn[j].rtn_addr);

        if (rtn == RTN_Invalid()) {
          cerr << "Unknwon"  << ":" << endl;
        } else {
          cerr << RTN_Name(rtn) << ":" << endl;
        }
      }
    }
    dump_instr_from_mem ((ADDRINT *)instr_map[i].new_ins_addr, instr_map[i].new_ins_addr);    
  }
}

void dump_instr_map_entry(int instr_map_entry)
{
  cerr << dec << instr_map_entry << ": ";
  cerr << " orig_ins_addr: " << hex << instr_map[instr_map_entry].orig_ins_addr;
  cerr << " new_ins_addr: " << hex << instr_map[instr_map_entry].new_ins_addr;
  cerr << " orig_targ_addr: " << hex << instr_map[instr_map_entry].orig_targ_addr;

  ADDRINT new_targ_addr;
  if (instr_map[instr_map_entry].new_targ_entry >= 0)
    new_targ_addr = instr_map[instr_map[instr_map_entry].new_targ_entry].new_ins_addr;
  else
    new_targ_addr = instr_map[instr_map_entry].orig_targ_addr;

  cerr << " new_targ_addr: " << hex << new_targ_addr;
  cerr << "    new instr:";
  dump_instr_from_mem((ADDRINT *)instr_map[instr_map_entry].encoded_ins, instr_map[instr_map_entry].new_ins_addr);
}

void dump_tc()
{
  char disasm_buf[2048];
  xed_decoded_inst_t new_xedd;
  ADDRINT address = (ADDRINT)&tc[0];
  unsigned int size = 0;

  while (address < (ADDRINT)&tc[tc_cursor]) {

    address += size;

    xed_decoded_inst_zero_set_mode(&new_xedd,&dstate); 

    xed_error_enum_t xed_code = xed_decode(&new_xedd, reinterpret_cast<UINT8*>(address), max_inst_len);          

    BOOL xed_ok = (xed_code == XED_ERROR_NONE);
    if (!xed_ok){
      cerr << "invalid opcode" << endl;
      return;
    }

    xed_format_context(XED_SYNTAX_INTEL, &new_xedd, disasm_buf, 2048, static_cast<UINT64>(address), 0, 0);

    cerr << "0x" << hex << address << ": " << disasm_buf <<  endl;

    size = xed_decoded_inst_get_length (&new_xedd); 
  }
}

int add_new_jmp_instr_entry(ADDRINT targ_addr, UINT64 inlinedIndex) {
  unsigned int new_size = 0;
  
  ADDRINT new_disp = (ADDRINT)targ_addr /*- 2*/;
  
  xed_encoder_instruction_t enc_instr;
  xed_encoder_request_t enc_req;
  
  xed_inst1(&enc_instr, dstate, XED_ICLASS_JMP, 64, xed_relbr(new_disp, 32));
  xed_bool_t convert_ok = xed_convert_to_encoder_request(&enc_req, &enc_instr);
  if (!convert_ok) {
    cerr << "conversion to encode request failed" << endl;
    return -1;
  }
      
  xed_error_enum_t xed_error = xed_encode (&enc_req, reinterpret_cast<UINT8*>(instr_map[num_of_instr_map_entries].encoded_ins), max_inst_len , &new_size);
  if (xed_error != XED_ERROR_NONE) {
    cerr << "ENCODE ERROR: " << xed_error_enum_t2str(xed_error) << endl;    
    return -1;
  } 
  
  instr_map[num_of_instr_map_entries].orig_ins_addr = 0;
  instr_map[num_of_instr_map_entries].new_ins_addr = (ADDRINT)&tc[tc_cursor];  // set an initial estimated addr in tc
  instr_map[num_of_instr_map_entries].orig_targ_addr = targ_addr; 
  instr_map[num_of_instr_map_entries].hasNewTargAddr = false;
  instr_map[num_of_instr_map_entries].new_targ_entry = -1;
  instr_map[num_of_instr_map_entries].size = new_size;  
  instr_map[num_of_instr_map_entries].inlinedIndex = inlinedIndex;  
  instr_map[num_of_instr_map_entries].category_enum = XED_CATEGORY_UNCOND_BR;
 
  num_of_instr_map_entries++;

  // update expected size of tc:
  tc_cursor += new_size;         

  if (num_of_instr_map_entries >= max_ins_count) {
    cerr << "out of memory for map_instr" << endl;
    return -1;
  }

  // debug print new encoded instr:
  if (KnobVerbose) {
    cerr << "    jmp new instr:";
    dump_instr_from_mem((ADDRINT *)instr_map[num_of_instr_map_entries-1].encoded_ins, instr_map[num_of_instr_map_entries-1].new_ins_addr);
  }

  return new_size;
}

int add_new_sub_instr_entry(UINT64 inlinedIndex) {
  unsigned int new_size = 0;
  
  xed_encoder_instruction_t enc_instr;
  xed_encoder_request_t enc_req;
  
  xed_inst2(&enc_instr, dstate, XED_ICLASS_SUB, 64, xed_reg(XED_REG_RSP), xed_imm0(8,32));
  xed_bool_t convert_ok = xed_convert_to_encoder_request(&enc_req, &enc_instr);
  if (!convert_ok) {
    cerr << "conversion to encode request failed" << endl;
    return -1;
  }
      
  xed_error_enum_t xed_error = xed_encode (&enc_req, reinterpret_cast<UINT8*>(instr_map[num_of_instr_map_entries].encoded_ins), max_inst_len , &new_size);
  if (xed_error != XED_ERROR_NONE) {
    cerr << "ENCODE ERROR: " << xed_error_enum_t2str(xed_error) << endl;    
    return -1;
  } 
 
  cout << "Adding sub RSP" << endl;
 
  instr_map[num_of_instr_map_entries].orig_ins_addr = 0;
  instr_map[num_of_instr_map_entries].new_ins_addr = (ADDRINT)&tc[tc_cursor];  // set an initial estimated addr in tc
  instr_map[num_of_instr_map_entries].orig_targ_addr = 0; 
  instr_map[num_of_instr_map_entries].hasNewTargAddr = false;
  instr_map[num_of_instr_map_entries].new_targ_entry = -1;
  instr_map[num_of_instr_map_entries].size = new_size;  
  instr_map[num_of_instr_map_entries].inlinedIndex = inlinedIndex;  
  instr_map[num_of_instr_map_entries].category_enum = XED_CATEGORY_PUSH;
 
  num_of_instr_map_entries++;

  // update expected size of tc:
  tc_cursor += new_size;         

  if (num_of_instr_map_entries >= max_ins_count) {
    cerr << "out of memory for map_instr" << endl;
    return -1;
  }

  // debug print new encoded instr:
  if (KnobVerbose) {
    cerr << "    call new instr:";
    dump_instr_from_mem((ADDRINT *)instr_map[num_of_instr_map_entries-1].encoded_ins, instr_map[num_of_instr_map_entries-1].new_ins_addr);
  }

  return new_size;
}


int add_new_add_instr_entry(UINT64 inlinedIndex) {
  unsigned int new_size = 0;
  
  xed_encoder_instruction_t enc_instr;
  xed_encoder_request_t enc_req;
  
  xed_inst2(&enc_instr, dstate, XED_ICLASS_ADD, 64, xed_reg(XED_REG_RSP), xed_imm0(8,32));
  xed_bool_t convert_ok = xed_convert_to_encoder_request(&enc_req, &enc_instr);
  if (!convert_ok) {
    cerr << "conversion to encode request failed" << endl;
    return -1;
  }
      
  xed_error_enum_t xed_error = xed_encode (&enc_req, reinterpret_cast<UINT8*>(instr_map[num_of_instr_map_entries].encoded_ins), max_inst_len , &new_size);
  if (xed_error != XED_ERROR_NONE) {
    cerr << "ENCODE ERROR: " << xed_error_enum_t2str(xed_error) << endl;    
    return -1;
  } 
 
  cout << "Adding add RSP" << endl;
 
  instr_map[num_of_instr_map_entries].orig_ins_addr = 0;
  instr_map[num_of_instr_map_entries].new_ins_addr = (ADDRINT)&tc[tc_cursor];  // set an initial estimated addr in tc
  instr_map[num_of_instr_map_entries].orig_targ_addr = 0; 
  instr_map[num_of_instr_map_entries].hasNewTargAddr = false;
  instr_map[num_of_instr_map_entries].new_targ_entry = -1;
  instr_map[num_of_instr_map_entries].size = new_size;  
  instr_map[num_of_instr_map_entries].inlinedIndex = inlinedIndex;  
  instr_map[num_of_instr_map_entries].category_enum = XED_CATEGORY_POP;
 
  num_of_instr_map_entries++;

  // update expected size of tc:
  tc_cursor += new_size;         

  if (num_of_instr_map_entries >= max_ins_count) {
    cerr << "out of memory for map_instr" << endl;
    return -1;
  }

  // debug print new encoded instr:
  if (KnobVerbose) {
    cerr << "    call new instr:";
    dump_instr_from_mem((ADDRINT *)instr_map[num_of_instr_map_entries-1].encoded_ins, instr_map[num_of_instr_map_entries-1].new_ins_addr);
  }

  return new_size;
}

int add_new_instr_entry(xed_decoded_inst_t *xedd, ADDRINT pc, unsigned int size, UINT64  inlinedIndex, ADDRINT override_targ_addr)
{

  // copy orig instr to instr map:
  ADDRINT orig_targ_addr = 0;

  if (xed_decoded_inst_get_length (xedd) != size) {
    cerr << "Invalid instruction decoding" << endl;
    return -1;
  }

  xed_uint_t disp_byts = xed_decoded_inst_get_branch_displacement_width(xedd);

  xed_int32_t disp;

  if (disp_byts > 0) { // there is a branch offset.
    disp = xed_decoded_inst_get_branch_displacement(xedd);
    orig_targ_addr = pc + xed_decoded_inst_get_length (xedd) + disp;  
  }

  // Converts the decoder request to a valid encoder request:
  xed_encoder_request_init_from_decode (xedd);

  unsigned int new_size = 0;

  xed_error_enum_t xed_error = xed_encode (xedd, reinterpret_cast<UINT8*>(instr_map[num_of_instr_map_entries].encoded_ins), max_inst_len , &new_size);
  if (xed_error != XED_ERROR_NONE) {
    cerr << "ENCODE ERROR: " << xed_error_enum_t2str(xed_error) << endl;    
    return -1;
  } 

  // add a new entry in the instr_map:

  instr_map[num_of_instr_map_entries].orig_ins_addr = pc;
  instr_map[num_of_instr_map_entries].new_ins_addr = (ADDRINT)&tc[tc_cursor];  // set an initial estimated addr in tc
  instr_map[num_of_instr_map_entries].orig_targ_addr = orig_targ_addr; 
  instr_map[num_of_instr_map_entries].hasNewTargAddr = false;
  instr_map[num_of_instr_map_entries].new_targ_entry = -1;
  instr_map[num_of_instr_map_entries].size = new_size;  
  instr_map[num_of_instr_map_entries].inlinedIndex = inlinedIndex;  
  instr_map[num_of_instr_map_entries].category_enum = xed_decoded_inst_get_category(xedd);

  // ADDED!!!!
  if(override_targ_addr!=0) {
    instr_map[num_of_instr_map_entries].orig_targ_addr = override_targ_addr; 
  }

  num_of_instr_map_entries++;

  // update expected size of tc:
  tc_cursor += new_size;         

  if (num_of_instr_map_entries >= max_ins_count) {
    cerr << "out of memory for map_instr" << endl;
    return -1;
  }


  // debug print new encoded instr:
  if (KnobVerbose) {
    cerr << "    new instr:";
    dump_instr_from_mem((ADDRINT *)instr_map[num_of_instr_map_entries-1].encoded_ins, instr_map[num_of_instr_map_entries-1].new_ins_addr);
    if(override_targ_addr!=0)
      cerr << " eventually the address will be " << override_targ_addr << endl;
  }

  return new_size;
}

int add_new_instr_entry(xed_decoded_inst_t *xedd, ADDRINT pc, UINT64  inlinedIndex, unsigned int size ) {
  return add_new_instr_entry(xedd, pc, size, inlinedIndex, 0);
}

int chain_all_direct_br_and_call_target_entries()
{
  for (int i=0; i < num_of_instr_map_entries; i++) {          

    if (instr_map[i].orig_targ_addr == 0)
      continue;

    if (instr_map[i].hasNewTargAddr)
      continue;

    for (int j = 0; j < num_of_instr_map_entries; j++) {

      if (j == i)
        continue;

      if ((instr_map[j].orig_ins_addr == instr_map[i].orig_targ_addr) && (instr_map[j].inlinedIndex == instr_map[i].inlinedIndex)) {
        if(KnobDebug3.Value()){
          cout << "j: " << j << " i: " << i << hex << " orig target: " << instr_map[i].orig_targ_addr << dec << " inlined index: " << instr_map[i].inlinedIndex << endl;
        }
        instr_map[i].hasNewTargAddr = true; 
        instr_map[i].new_targ_entry = j;
        break;
      }
    }
  }

  return 0;
}

int fix_rip_displacement(int instr_map_entry) 
{

  xed_decoded_inst_t xedd;
  xed_decoded_inst_zero_set_mode(&xedd,&dstate); 

  xed_error_enum_t xed_code = xed_decode(&xedd, reinterpret_cast<UINT8*>(instr_map[instr_map_entry].encoded_ins), max_inst_len);
  if (xed_code != XED_ERROR_NONE) {
    cerr << "ERROR: xed decode failed for instr at: " << "0x" << hex << instr_map[instr_map_entry].new_ins_addr << endl;
    return -1;
  }

  unsigned int memops = xed_decoded_inst_number_of_memory_operands(&xedd);

  if (instr_map[instr_map_entry].orig_targ_addr != 0)  // a direct jmp or call instruction.
    return 0;

  //cerr << "Memory Operands" << endl;
  bool isRipBase = false;
  xed_reg_enum_t base_reg = XED_REG_INVALID;
  xed_int64_t disp = 0;
  for(unsigned int i=0; i < memops ; i++)   {

    base_reg = xed_decoded_inst_get_base_reg(&xedd,i);
    disp = xed_decoded_inst_get_memory_displacement(&xedd,i);

    if (base_reg == XED_REG_RIP) {
      isRipBase = true;
      break;
    }

  }

  if (!isRipBase)
    return 0;


  xed_int64_t new_disp = 0;
  xed_uint_t new_disp_byts = 4;   // set maximal num of byts for now.

  unsigned int orig_size = xed_decoded_inst_get_length (&xedd);

  // modify rip displacement. use direct addressing mode: 
  new_disp = instr_map[instr_map_entry].orig_ins_addr + disp + orig_size; // xed_decoded_inst_get_length (&xedd_orig);
  xed_encoder_request_set_base0 (&xedd, XED_REG_INVALID);

  //Set the memory displacement using a bit length 
  xed_encoder_request_set_memory_displacement (&xedd, new_disp, new_disp_byts);

  unsigned int size = XED_MAX_INSTRUCTION_BYTES;
  unsigned int new_size = 0;

  // Converts the decoder request to a valid encoder request:
  xed_encoder_request_init_from_decode (&xedd);

  xed_error_enum_t xed_error = xed_encode (&xedd, reinterpret_cast<UINT8*>(instr_map[instr_map_entry].encoded_ins), size , &new_size); // &instr_map[i].size
  if (xed_error != XED_ERROR_NONE) {
    cerr << "ENCODE ERROR: " << xed_error_enum_t2str(xed_error) << endl;
    dump_instr_map_entry(instr_map_entry); 
    return -1;
  }       

  if (KnobVerbose) {
    dump_instr_map_entry(instr_map_entry);
  }

  return new_size;
}

int fix_direct_br_call_to_orig_addr(int instr_map_entry)
{
  xed_decoded_inst_t xedd;
  xed_decoded_inst_zero_set_mode(&xedd,&dstate); 

  xed_error_enum_t xed_code = xed_decode(&xedd, reinterpret_cast<UINT8*>(instr_map[instr_map_entry].encoded_ins), max_inst_len);
  if (xed_code != XED_ERROR_NONE) {
    cerr << "ERROR: xed decode failed for instr at: " << "0x" << hex << instr_map[instr_map_entry].new_ins_addr << endl;
    return -1;
  }

  xed_category_enum_t category_enum = xed_decoded_inst_get_category(&xedd);

  if (category_enum != XED_CATEGORY_CALL && category_enum != XED_CATEGORY_UNCOND_BR) {

    cerr << "ERROR: Invalid direct jump from translated code to original code in rotuine: " 
      << RTN_Name(RTN_FindByAddress(instr_map[instr_map_entry].orig_ins_addr)) << endl;
    dump_instr_map_entry(instr_map_entry);
    return -1;
  }

  // check for cases of direct jumps/calls back to the orginal target address:
  if (instr_map[instr_map_entry].new_targ_entry >= 0) {
    cerr << "ERROR: Invalid jump or call instruction" << endl;
    return -1;
  }

  unsigned int ilen = XED_MAX_INSTRUCTION_BYTES;
  unsigned int olen = 0;


  xed_encoder_instruction_t  enc_instr;

  ADDRINT new_disp = (ADDRINT)&instr_map[instr_map_entry].orig_targ_addr - 
    instr_map[instr_map_entry].new_ins_addr - 
    xed_decoded_inst_get_length (&xedd);

  if (category_enum == XED_CATEGORY_CALL)
    xed_inst1(&enc_instr, dstate, 
        XED_ICLASS_CALL_NEAR, 64,
        xed_mem_bd (XED_REG_RIP, xed_disp(new_disp, 32), 64));

  if (category_enum == XED_CATEGORY_UNCOND_BR)
    xed_inst1(&enc_instr, dstate, 
        XED_ICLASS_JMP, 64,
        xed_mem_bd (XED_REG_RIP, xed_disp(new_disp, 32), 64));


  xed_encoder_request_t enc_req;

  xed_encoder_request_zero_set_mode(&enc_req, &dstate);
  xed_bool_t convert_ok = xed_convert_to_encoder_request(&enc_req, &enc_instr);
  if (!convert_ok) {
    cerr << "conversion to encode request failed" << endl;
    return -1;
  }


  xed_error_enum_t xed_error = xed_encode(&enc_req, reinterpret_cast<UINT8*>(instr_map[instr_map_entry].encoded_ins), ilen, &olen);
  if (xed_error != XED_ERROR_NONE) {
    cerr << "ENCODE ERROR: " << xed_error_enum_t2str(xed_error) << endl;
    dump_instr_map_entry(instr_map_entry); 
    return -1;
  }

  // handle the case where the original instr size is different from new encoded instr:
  if (olen != xed_decoded_inst_get_length (&xedd)) {

    new_disp = (ADDRINT)&instr_map[instr_map_entry].orig_targ_addr - 
      instr_map[instr_map_entry].new_ins_addr - olen;

    if (category_enum == XED_CATEGORY_CALL)
      xed_inst1(&enc_instr, dstate, 
          XED_ICLASS_CALL_NEAR, 64,
          xed_mem_bd (XED_REG_RIP, xed_disp(new_disp, 32), 64));

    if (category_enum == XED_CATEGORY_UNCOND_BR)
      xed_inst1(&enc_instr, dstate, 
          XED_ICLASS_JMP, 64,
          xed_mem_bd (XED_REG_RIP, xed_disp(new_disp, 32), 64));


    xed_encoder_request_zero_set_mode(&enc_req, &dstate);
    xed_bool_t convert_ok = xed_convert_to_encoder_request(&enc_req, &enc_instr);
    if (!convert_ok) {
      cerr << "conversion to encode request failed" << endl;
      return -1;
    }

    xed_error = xed_encode (&enc_req, reinterpret_cast<UINT8*>(instr_map[instr_map_entry].encoded_ins), ilen , &olen);
    if (xed_error != XED_ERROR_NONE) {
      cerr << "ENCODE ERROR: " << xed_error_enum_t2str(xed_error) << endl;
      dump_instr_map_entry(instr_map_entry);
      return -1;
    }   
  }


  // debug prints:
  if (KnobVerbose) {
    dump_instr_map_entry(instr_map_entry); 
  }

  instr_map[instr_map_entry].hasNewTargAddr = true;
  return olen;  
}

int fix_direct_br_call_displacement(int instr_map_entry) 
{         

  xed_decoded_inst_t xedd;
  xed_decoded_inst_zero_set_mode(&xedd,&dstate); 

  xed_error_enum_t xed_code = xed_decode(&xedd, reinterpret_cast<UINT8*>(instr_map[instr_map_entry].encoded_ins), max_inst_len);
  if (xed_code != XED_ERROR_NONE) {
    cerr << "ERROR: xed decode failed for instr at: " << "0x" << hex << instr_map[instr_map_entry].new_ins_addr << endl;
    return -1;
  }

  xed_int32_t  new_disp = 0;  
  unsigned int size = XED_MAX_INSTRUCTION_BYTES;
  unsigned int new_size = 0;


  xed_category_enum_t category_enum = xed_decoded_inst_get_category(&xedd);

  if (category_enum != XED_CATEGORY_CALL && category_enum != XED_CATEGORY_COND_BR && category_enum != XED_CATEGORY_UNCOND_BR) {
    cerr << "ERROR: unrecognized branch displacement" << endl;
    return -1;
  }

  // fix branches/calls to original targ addresses:
  if (instr_map[instr_map_entry].new_targ_entry < 0) {
    int rc = fix_direct_br_call_to_orig_addr(instr_map_entry);
    return rc;
  }

  ADDRINT new_targ_addr;    
  new_targ_addr = instr_map[instr_map[instr_map_entry].new_targ_entry].new_ins_addr;

  new_disp = (new_targ_addr - instr_map[instr_map_entry].new_ins_addr) - instr_map[instr_map_entry].size; // orig_size;

  xed_uint_t   new_disp_byts = 4; // num_of_bytes(new_disp);  ???

  // the max displacement size of loop instructions is 1 byte:
  xed_iclass_enum_t iclass_enum = xed_decoded_inst_get_iclass(&xedd);
  if (iclass_enum == XED_ICLASS_LOOP ||  iclass_enum == XED_ICLASS_LOOPE || iclass_enum == XED_ICLASS_LOOPNE) {
    new_disp_byts = 1;
  }

  // the max displacement size of jecxz instructions is ???:
  xed_iform_enum_t iform_enum = xed_decoded_inst_get_iform_enum (&xedd);
  if (iform_enum == XED_IFORM_JRCXZ_RELBRb){
    new_disp_byts = 1;
  }

  // Converts the decoder request to a valid encoder request:
  xed_encoder_request_init_from_decode (&xedd);

  //Set the branch displacement:
  xed_encoder_request_set_branch_displacement (&xedd, new_disp, new_disp_byts);

  xed_uint8_t enc_buf[XED_MAX_INSTRUCTION_BYTES];
  unsigned int max_size = XED_MAX_INSTRUCTION_BYTES;

  xed_error_enum_t xed_error = xed_encode (&xedd, enc_buf, max_size , &new_size);
  if (xed_error != XED_ERROR_NONE) {
    cerr << "ENCODE ERROR: " << xed_error_enum_t2str(xed_error) <<  endl;
    char buf[2048];   
    xed_format_context(XED_SYNTAX_INTEL, &xedd, buf, 2048, static_cast<UINT64>(instr_map[instr_map_entry].orig_ins_addr), 0, 0);
    cerr << " instr: " << "0x" << hex << instr_map[instr_map_entry].orig_ins_addr << " : " << buf <<  endl;
    return -1;
  }   

  new_targ_addr = instr_map[instr_map[instr_map_entry].new_targ_entry].new_ins_addr;

  new_disp = new_targ_addr - (instr_map[instr_map_entry].new_ins_addr + new_size);  // this is the correct displacemnet.

  //Set the branch displacement:
  xed_encoder_request_set_branch_displacement (&xedd, new_disp, new_disp_byts);

  xed_error = xed_encode (&xedd, reinterpret_cast<UINT8*>(instr_map[instr_map_entry].encoded_ins), size , &new_size); // &instr_map[i].size
  if (xed_error != XED_ERROR_NONE) {
    cerr << "ENCODE ERROR: " << xed_error_enum_t2str(xed_error) << endl;
    dump_instr_map_entry(instr_map_entry);
    return -1;
  }       

  //debug print of new instruction in tc:
  if (KnobVerbose) {
    dump_instr_map_entry(instr_map_entry);
  }

  return new_size;
}     

int fix_instructions_displacements()
{
  // fix displacemnets of direct branch or call instructions:

  int size_diff = 0;  

  do {

    size_diff = 0;

    if (KnobVerbose) {
      cerr << "starting a pass of fixing instructions displacements: " << endl;
    }

    for (int i=0; i < num_of_instr_map_entries; i++) {

      instr_map[i].new_ins_addr += size_diff;

      int rc = 0;

      // fix rip displacement:      
      rc = fix_rip_displacement(i);
      if (rc < 0)
        return -1;

      if (rc > 0) { // this was a rip-based instruction which was fixed.

        if (instr_map[i].size != (unsigned int)rc) {
          size_diff += (rc - instr_map[i].size);          
          instr_map[i].size = (unsigned int)rc;               
        }

        continue;   
      }

      // check if it is a direct branch or a direct call instr:
      if (instr_map[i].orig_targ_addr == 0) {
        continue;  // not a direct branch or a direct call instr.
      }


      // fix instr displacement:      
      rc = fix_direct_br_call_displacement(i);
      if (rc < 0)
        return -1;

      if (instr_map[i].size != (unsigned int)rc) {
        size_diff += (rc - instr_map[i].size);
        instr_map[i].size = (unsigned int)rc;
      }

    }  // end int i=0; i ..

  } while (size_diff != 0);

  return 0;
}

BBLEDGEINFO* update_edge_map(BBLEDGEINFO* bblEdgeMapElem, UINT64 edgeCount, int index){
  if (bblEdgeMapElem){
    if(edgeCount > bblEdgeMapElem->edgeCount){
      bblEdgeMapElem->maxEdgeIndex = index;
      bblEdgeMapElem->edgeCount = edgeCount;
    }
  }
  else{
    bblEdgeMapElem = new BBLEDGEINFO;
    bblEdgeMapElem->maxEdgeIndex = index;
    bblEdgeMapElem->edgeCount = edgeCount;
  }
  return bblEdgeMapElem;
}

std::vector<BBLINFO> get_fixed_bbl_list_with_inline(std::vector<BBLINFO> &bblOrder, RTN &rtn) {

  std::vector<BBLINFO> bblOptOrder;

  for(UINT64 i=0; i < bblOrder.size(); ++i){

		bblOptOrder.push_back(bblOrder[i]);


	  if(bblOrder[i].isCall && i+1<bblOrder.size() && bblOrder[i].fallThroughCount == 0) {


      RTN calledRtn = RTN_FindByAddress(bblOrder[i].takenAddr);
      if(RTN_FindByAddress(bblOrder[i].takenAddr) != rtn) { // not recursive
        RTN_Open(calledRtn);
        UINT64 rtnHead = (UINT64)INS_Address(RTN_InsHead(calledRtn));
        UINT64 takenAddr = (UINT64)bblOrder[i].takenAddr;
  
        if(rtnHead == takenAddr){ // we inline only entire rtns

          BBLINFO q;

          q.start = INS_Address(RTN_InsHead(calledRtn));
          q.end = INS_Address(RTN_InsTail(calledRtn));
          if(bblOrder.size() >= i+1)
            q.fallThroughAddr = bblOrder[i+1].start;         
          else
            q.fallThroughAddr = 0;         
             
          q.fallThroughCount = SIZE_MAX;          
          q.takenAddr = 0;          
          q.takenCount = 0;         
          q.isCall = 0; 
            
          BOOL hasRetEnd = false;
		      BOOL branchOutside = false;
		      BOOL callInside = false;
          INS ins;
          ADDRINT rtnOffset = RTN_Address(calledRtn) - mainExeImageLowAddr;
          if(rtnList.count(rtnOffset) > 0){ // inline only profiled rtns
            if (q.end - q.start < KnobInlineSize.Value()){ 
              for(ins=RTN_InsHead(calledRtn); ins != RTN_InsTail(calledRtn); ins = INS_Next(ins)){
                if(INS_IsDirectBranch(ins)) {
                  if(INS_DirectBranchOrCallTargetAddress(ins)<INS_Address(RTN_InsHead(calledRtn)) || INS_DirectBranchOrCallTargetAddress(ins)>INS_Address(RTN_InsTail(calledRtn))) {
                    branchOutside = true;
                  }
                }
                if(INS_IsCall(ins)) {
                  callInside = true;
                } 
              }
              xed_decoded_inst_t *xedd2 = INS_XedDec(ins);
            
              xed_category_enum_t category_enum = xed_decoded_inst_get_category(xedd2);

              if (category_enum == XED_CATEGORY_RET){
                hasRetEnd = true;
              }
            }
          }
          if(hasRetEnd && !branchOutside && !callInside){
            cout << "inline RTN " << RTN_Name(calledRtn) << endl;
            if(inlinedIndexList.count(RTN_Address(calledRtn)) == 0){
              inlinedIndexList[RTN_Address(calledRtn)] = 0;
            }
            if(KnobDebug3.Value()){
              cout << "INLINED INST:" << endl;
              for(ins=RTN_InsHead(calledRtn); ins != RTN_InsTail(calledRtn); ins = INS_Next(ins)){
                  cout << " 0x" << hex << INS_Address(ins) << ": " << INS_Disassemble(ins) << dec <<  endl;
              }
              cout << "  0x" << hex << INS_Address(ins) << ": " << INS_Disassemble(ins) << dec <<  endl;
            }
            
            bblOptOrder.push_back(q);
          }
          
        }

        RTN_Close(calledRtn);

      }
	  }
  }

  return bblOptOrder;
}

std::vector<BBLINFO> get_fixed_bbl_list(BBLLIST &bblList, RTN &rtn) {
  std::vector<BBLINFO> bblOptOrder;
  BBLINFO *old_bbl = NULL;
  // First iteration - BBL order is sequential, skipping the BBLs which are contained in other BBLs (not inlining them).
  // Although skipping them, the new BBLs order will take the highest count within "big" BBL to reorder.  
  for (std::map<ADDRINT, BBLINFO*>::iterator it=bblList.begin(); it!=bblList.end(); ++it) {
  
    if(old_bbl!=NULL && it->second->end <= old_bbl->end) {
      
      if(KnobDebug) cout << "BBL IS CONTAINED" << endl;

      if(it->second->fallThroughCount > old_bbl->fallThroughCount || it->second->takenCount > old_bbl->takenCount) {
        if(KnobDebug) cout << "SWITCHING COUNT" << endl;

        bblOptOrder.back().fallThroughCount = it->second->fallThroughCount;
        bblOptOrder.back().takenCount = it->second->takenCount;
        bblOptOrder.back().isCall = it->second->isCall;
      }
    }
    else {
      BBLINFO r;      
      
      // Copy BBLINFO
      r.start = it->second->start;          
      r.end = it->second->end;          
      r.fallThroughAddr = it->second->fallThroughAddr;          
      r.fallThroughCount = it->second->fallThroughCount;          
      r.takenAddr = it->second->takenAddr;          
      r.takenCount = it->second->takenCount;          
      r.isCall = it->second->isCall;          

      std::map<ADDRINT, BBLINFO*>::iterator it2 = it;
      it2++;
    
      if(((r.fallThroughAddr<INS_Address(RTN_InsHead(rtn)) || r.fallThroughAddr>INS_Address(RTN_InsTail(rtn))) && r.isCall==false)) {
        if((it2)!=bblList.end()) {
        r.fallThroughAddr = (it2)->second->start;
        }
      
        r.takenAddr = 0;
      }

      bblOptOrder.push_back(r);
      old_bbl = it->second;

    }
  }

  // BBLs can also not be fully on each other... Let's eliminate the overlapping
  for (std::vector<BBLINFO>::iterator it=bblOptOrder.begin(); it!=bblOptOrder.end(); ++it) {
    
    if((it+1)!=bblOptOrder.end() && (*(it+1)).start < (*it).end) {
      // In this case merge the BBLs and do not optimize them

      if(KnobDebug) cout << "MERGING" << endl;

      (*it).end = (*(it+1)).end;
      (*it).fallThroughAddr = 0;
      (*it).takenAddr = 0;
      (*it).fallThroughCount = 0;
      (*it).takenCount = 0;
      (*it).isCall = false;

      bblOptOrder.erase(it+1); // it is not valid after erase
      it = bblOptOrder.begin();
    }

  }


  // Check for missing BBLs
  std::vector<BBLINFO>::iterator it0 = bblOptOrder.begin();
  bool flag=false;
  ADDRINT fake_start=0, fake_end=0;
  for (INS ins = RTN_InsHead(rtn); INS_Valid(ins); ins = INS_Next(ins)) {

    if(KnobDebug) cout << "INS " << hex << INS_Address(ins) << "..... " << (*it0).start << " - " << (*it0).end << "?" << endl;

    if(((*it0).start<=INS_Address(ins) && INS_Address(ins)<=(*it0).end) || !INS_Valid(INS_Next(ins)) ) {
      flag=false;
      
      if(fake_start!=0) {
        fake_end = INS_Address(INS_Prev(ins));

        BBLINFO r;
        r.start = fake_start;
        r.end = fake_end;
        r.fallThroughAddr = 0;
        r.fallThroughCount = 0;
        r.takenAddr = 0;
        r.takenCount = 0;
        r.isCall = false;

        if(KnobDebug) cout << "ADDED FAKE BBL " << hex << r.start << " - " << r.end << endl;

        bblOptOrder.insert(it0, r); // After insert the iterator is invalid
        //it0 = bblOptOrder.begin();
        for(it0 = bblOptOrder.begin(); (*it0).start!=r.start; ++it0) { }
        it0++;
    

        fake_start = 0;
        fake_end = 0;
      }
    }
    else {
      if(flag==false) {
        it0++;
        ins = INS_Prev(ins);
        flag = true;
      }
      else {
        if(KnobDebug) cout << "MISSING!#!## " << hex << INS_Address(ins) << endl;
        if(fake_start==0) {
          if(KnobDebug) cout << "FAKE ADDRESS SET" << endl;
          fake_start = INS_Address(ins);
        }
      }
    }
    
  }
  return bblOptOrder;
}
std::vector<BBLINFO> optimize_bbl_order(BBLLIST &bblList, RTN &rtn) {
  RTN_Open( rtn );              
  // get all of the rtn bbls for optimization
  std::vector<BBLINFO> bblOptOrder = get_fixed_bbl_list(bblList, rtn);
  RTN_Close(rtn);     

  if(Opt3Knob.Value()){
  //get rtn bbls with inlined bbls from different rtns
  bblOptOrder = get_fixed_bbl_list_with_inline(bblOptOrder, rtn);
  } 
 

  // Iteration 2
  //
  
  if(KnobDebug) {
    cout << "BEFORE REORDER" << endl;
    for (std::vector<BBLINFO>::iterator orderIt = bblOptOrder.begin(); orderIt!=bblOptOrder.end(); ++orderIt) {
      cout << hex << "Start=0x" << (*orderIt).start << ", End=0x" << (*orderIt).end << 
               ", FallThroughAddr=0x" << (*orderIt).fallThroughAddr << ", TakenAddr=0x" << (*orderIt).takenAddr << 
               dec << ", FallThroughCount=" << (*orderIt).fallThroughCount << ", TakenCount=" << (*orderIt).takenCount << endl; 
    }
  }


  vector<BBLINFO> bblOptOrderNew(bblOptOrder);

  // GIL METHOD 2!!!
  // looking for loops between bbls jumps, and reorder accordingly
  if(KnobOptMethod.Value()==2) {
    UINT64 max_val = 0;
    int max_order[11];
    for(int i=0; i<11; i++) {
      max_order[i] = -1;
    }
    
    for (UINT64 i = 0; i < bblOptOrder.size(); ++i) {
      UINT64 bbl_i = i;
      int order[11];
      ADDRINT nextBBL = 0;
      UINT64 val = 0;		

      for(int t=0; t<11; t++) {
        order[t] = -1;
      }
      
      for(int q = 0; q<10; q++) {
        order[q] = bbl_i;

        bool quit=false;

        if(q>0) {
          for(int k=0; k<q; k++) {
            if(order[q] == order[k]) {
              quit = true;
              order[q] = -1;
              break;
            }
          }
        }

        if(quit) break;

        if(bblOptOrder[bbl_i].takenCount > bblOptOrder[bbl_i].fallThroughCount) {
          nextBBL = bblOptOrder[bbl_i].takenAddr;
          val+=bblOptOrder[bbl_i].takenCount;
        }
        else {
          nextBBL = bblOptOrder[bbl_i].fallThroughAddr;
          val+=bblOptOrder[bbl_i].fallThroughCount;
        }

        for (UINT64 j = 0; j < bblOptOrder.size(); ++j) {
          if(bblOptOrder[j].start == nextBBL) {
            bbl_i = j;

            break;
          }
        }
      }

      if(max_val < val) {
        max_val = val;

        for(int t=0; t<11; t++) {
          max_order[t] = order[t];
        }		

      }
    }

    cout << max_val << endl;
    for(int t=0; t<11; t++) {
      cout << max_order[t] << ", ";
    }		
    cout << endl;



    int last_p;
    for(last_p=0; last_p<11; last_p++) {
      if(max_order[last_p]==-1) break;
      if(max_order[last_p]==0) break;

      bblOptOrderNew[last_p+1] = bblOptOrder[max_order[last_p]];
    }	
    last_p++;

    for(int t=0; t<(int)bblOptOrder.size(); t++) {
      bool cont = false;
      
      for(int k=0; k<11; k++) {
        if(t==max_order[k] || t==0) {
          cont = true;
          break;
        }
      }

      if(cont) continue;

      bblOptOrderNew[last_p] = bblOptOrder[t];
      
      last_p++;
    }

    for(UINT64 i = 0; i < bblOptOrderNew.size(); i++){
      for(UINT64 k = 0; k < bblOptOrderNew.size(); ++k){
      if (bblOptOrderNew[i].start == bblOptOrder[k].start && bblOptOrderNew[i].end == bblOptOrder[k].end){
        cout << k << ", ";
        break;
      }
      }
    }
    cout << endl;
  }


  // GIL METHOD 1!!!
  // switching taken and fallthrough bbls if adjacent
  if(KnobOptMethod.Value()==1) {

    vector<BOOL> bblOptOrderValid(bblOptOrder.size(), false);

    for (UINT64 i = 0; i < bblOptOrder.size(); ++i) {
      bool bblNoFit = false;
      bool noSwap = true;

      for(int k=0; k<=2; k++) {
        if(bblOptOrder[i+k].fallThroughCount==0 && bblOptOrder[i+k].takenCount==0 && bblOptOrder[i+k].takenAddr==0 && bblOptOrder[i+k].fallThroughAddr==0) {
          bblNoFit = true;
        }
      }

      if(!bblNoFit) {

        if(bblOptOrder[i].fallThroughAddr == bblOptOrder[i+1].start && bblOptOrder[i].takenAddr == bblOptOrder[i+2].start) {
          if(bblOptOrder[i].fallThroughCount * 100 < bblOptOrder[i].takenCount) {
            cout << "SWAP!" << endl;

            bblOptOrderNew[i] = bblOptOrder[i];
            bblOptOrderNew[i+1] = bblOptOrder[i+2];
            bblOptOrderNew[i+2] = bblOptOrder[i+1];

            noSwap = false;

            i+=2;
          }
        }
      }

      if(noSwap) {
        bblOptOrderNew[i] = bblOptOrder[i];
      }
    }

    for(UINT64 i = 0; i < bblOptOrderNew.size(); i++){
      for(UINT64 k = 0; k < bblOptOrderNew.size(); ++k){
      if (bblOptOrderNew[i].start == bblOptOrder[k].start && bblOptOrderNew[i].end == bblOptOrder[k].end){
        cout << k << ", ";
        break;
      }
      }
    }
    cout << endl;
  }

  // DANIEL METHOD!
  // creating an inverse edge map and trying to reorder accordingly
  if(KnobOptMethod.Value()==0) {


    //create bbl edge map
    vector<BBLEDGEINFO*> bblEdgeMap(bblOptOrder.size(), NULL);
    for (UINT64 i = 0; i < bblOptOrder.size(); ++i) {
      int pos_fT=-1, pos_t=-1;

      for (UINT64 j = 0; j < bblOptOrder.size(); ++j) {
        if(bblOptOrder[j].fallThroughCount==0 && bblOptOrder[j].takenCount==0 && bblOptOrder[j].takenAddr==0 && bblOptOrder[j].fallThroughAddr==0){
          continue;
        }
        if(i != j){ 
          if(bblOptOrder[j].start == bblOptOrder[i].fallThroughAddr) {
            pos_fT = j;
          }
          if(bblOptOrder[j].start == bblOptOrder[i].takenAddr) {
            pos_t = j;
          }
        }
      }
      if (pos_t != -1){
        bblEdgeMap[pos_t] = update_edge_map(bblEdgeMap[pos_t], bblOptOrder[i].takenCount, i);
      }
      if (pos_fT != -1){
        bblEdgeMap[pos_fT] = update_edge_map(bblEdgeMap[pos_fT], bblOptOrder[i].fallThroughCount, i);
      }
    }
    UINT64 absolutMaxEdge = 0;
    for(UINT64 i = 0; i < bblEdgeMap.size(); i++){
      if(bblEdgeMap[i]){
        if(bblEdgeMap[i]->edgeCount > absolutMaxEdge){
            absolutMaxEdge = bblEdgeMap[i]->edgeCount;
        }
      }
    }

    vector<BOOL> bblLocalEdgeMap(bblOptOrder.size(), false);
    for (UINT64 i = 0; i < bblLocalEdgeMap.size(); ++i) {
      for(UINT64 j = 0; j < bblOptOrder.size(); ++j){
        if(bblOptOrder[i].takenAddr == bblOptOrder[j].start){
          bblLocalEdgeMap[i] = true;
          break;
        }
      }
    }

    vector<UINT64> bblSwappedOut(bblOptOrder.size(), 0); //0 - not moved, 1 - swapped out, 2 - allready placed , 3 - DO NOT MOVE
    bblSwappedOut[0] = 3;
    UINT64 currentLastIndex = 0;
    for (UINT64 i = 0; i < bblOptOrder.size(); ++i) {
      if (bblSwappedOut[i+1] == 3) {
        continue;
      }
      int swapIndex = -1;
      UINT64  maxEdge = 0;
      for (UINT64 j = 0; j < bblEdgeMap.size(); ++j) {
        if (bblEdgeMap[j]){
          if ((bblEdgeMap[j]->maxEdgeIndex == currentLastIndex) && (bblEdgeMap[j]->edgeCount > maxEdge) && (bblEdgeMap[j]->edgeCount > absolutMaxEdge / OPT_FACTOR) && (bblSwappedOut[j] < 2)){
            swapIndex = j;
            maxEdge = bblEdgeMap[j]->edgeCount;
          }
        }
      }
      // dont swap with last bbl
      if ((swapIndex != static_cast<int>(bblOptOrder.size()) - 1) && (swapIndex != -1)){
        if(KnobDebug2) cout << "PLACING " << swapIndex << " IN " << i + 1 << endl;
        if(KnobDebug2) cout << "LAST INDEX " << currentLastIndex << endl;
      }
      else{
        swapIndex = -1;
        UINT64 maxEdge = 0;
        for(UINT64 k = 0; k < bblEdgeMap.size(); ++k){
          if(bblEdgeMap[k]){  
            if ((bblSwappedOut[bblEdgeMap[k]->maxEdgeIndex] < 2) && bblLocalEdgeMap[bblEdgeMap[k]->maxEdgeIndex]){ 
              if ((maxEdge < bblEdgeMap[k]->edgeCount) && (bblEdgeMap[k]->edgeCount > absolutMaxEdge / OPT_FACTOR)){
                maxEdge = bblEdgeMap[k]->edgeCount;
                swapIndex = bblEdgeMap[k]->maxEdgeIndex;
              }
            }
          }
        }
        if(swapIndex == -1){
          for(UINT64 k = 0; k < bblSwappedOut.size() - 1; ++k){
            if(bblSwappedOut[k] == 1){
              swapIndex = k;
              break;
            }
          }
        }
        if(KnobDebug2) cout << "PLACING FROM SWAPPED " << swapIndex << " IN " << i + 1 << endl;
        if(KnobDebug2) cout << "LAST INDEX " << currentLastIndex << endl;
      }
      if(swapIndex != -1){
        bblSwappedOut[swapIndex] = 2;
        if(bblSwappedOut[i+1] == 0){
          bblSwappedOut[i+1] = 1;
        }
        bblOptOrderNew[i+1] = bblOptOrder[swapIndex];
        currentLastIndex = swapIndex;
      }
      else{
          currentLastIndex = i+1;
          bblSwappedOut[i+1] = 2;
      }
      
    }
    if(KnobDebug2){
      for(UINT64 i = 0; i < bblOptOrderNew.size(); i++){
        for(UINT64 k = 0; k < bblOptOrderNew.size(); ++k){
          if (bblOptOrderNew[i].start == bblOptOrder[k].start && bblOptOrderNew[i].end == bblOptOrder[k].end){
            cout << k << ", ";
            break;
          }
        }
      }
      cout << endl;
    }


  } 
  
  rtnIndex++;
  return bblOptOrderNew;     
}


int revert_cond_jump(INS ins_tail, ADDRINT new_addr, UINT64  inlinedIndex) {
  unsigned int new_size = 0;
  
  xed_decoded_inst_t *xedd = INS_XedDec(ins_tail);
  
  xed_category_enum_t category_enum = xed_decoded_inst_get_category(xedd);

  if (category_enum == XED_CATEGORY_UNCOND_BR){ 
    if(KnobDebug2) cout << "REMOVED JUMP" << endl;
    return 2;
  }
  
  if (category_enum == XED_CATEGORY_CALL){
    cout << "removeing inlined call" <<endl;
    return 3;
  }

  if (category_enum != XED_CATEGORY_COND_BR)
    return -1;
  
  xed_iclass_enum_t iclass_enum = xed_decoded_inst_get_iclass(xedd);

  if (iclass_enum == XED_ICLASS_JRCXZ)
    return -1;    // do not revert JRCXZ

  xed_iclass_enum_t retverted_iclass;

  switch (iclass_enum) {

    case XED_ICLASS_JB:
      retverted_iclass = XED_ICLASS_JNB;    
      break;

    case XED_ICLASS_JBE:
      retverted_iclass = XED_ICLASS_JNBE;
      break;

    case XED_ICLASS_JL:
      retverted_iclass = XED_ICLASS_JNL;
      break;
  
    case XED_ICLASS_JLE:
      retverted_iclass = XED_ICLASS_JNLE;
      break;

    case XED_ICLASS_JNB: 
      retverted_iclass = XED_ICLASS_JB;
      break;

    case XED_ICLASS_JNBE: 
      retverted_iclass = XED_ICLASS_JBE;
      break;

    case XED_ICLASS_JNL:
    retverted_iclass = XED_ICLASS_JL;
      break;

    case XED_ICLASS_JNLE:
      retverted_iclass = XED_ICLASS_JLE;
      break;

    case XED_ICLASS_JNO:
      retverted_iclass = XED_ICLASS_JO;
      break;

    case XED_ICLASS_JNP: 
      retverted_iclass = XED_ICLASS_JP;
      break;

    case XED_ICLASS_JNS: 
      retverted_iclass = XED_ICLASS_JS;
      break;

    case XED_ICLASS_JNZ:
      retverted_iclass = XED_ICLASS_JZ;
      break;

    case XED_ICLASS_JO:
      retverted_iclass = XED_ICLASS_JNO;
      break;

    case XED_ICLASS_JP: 
      retverted_iclass = XED_ICLASS_JNP;
      break;

    case XED_ICLASS_JS: 
      retverted_iclass = XED_ICLASS_JNS;
      break;

    case XED_ICLASS_JZ:
      retverted_iclass = XED_ICLASS_JNZ;
      break;

    default:
      return -1;
  }

  // Converts the decoder request to a valid encoder request:
  xed_encoder_request_init_from_decode (xedd);

  xed_encoder_instruction_t enc_instr;
  xed_encoder_request_t enc_req;
  
  xed_inst1(&enc_instr, dstate, retverted_iclass, 64, xed_relbr(new_addr, 32));
  xed_bool_t convert_ok = xed_convert_to_encoder_request(&enc_req, &enc_instr);
  if (!convert_ok) {
    cerr << "conversion to encode request failed" << endl;
    return -1;
  }
      
  xed_error_enum_t xed_error = xed_encode (&enc_req, reinterpret_cast<UINT8*>(instr_map[num_of_instr_map_entries].encoded_ins), max_inst_len , &new_size);
  if (xed_error != XED_ERROR_NONE) {
    cerr << "ENCODE ERROR: " << xed_error_enum_t2str(xed_error) << endl;    
    return -1;
  } 

  instr_map[num_of_instr_map_entries].orig_ins_addr = INS_Address(ins_tail);
  instr_map[num_of_instr_map_entries].new_ins_addr = (ADDRINT)&tc[tc_cursor];  // set an initial estimated addr in tc
  instr_map[num_of_instr_map_entries].orig_targ_addr = new_addr; 
  instr_map[num_of_instr_map_entries].hasNewTargAddr = false;
  instr_map[num_of_instr_map_entries].new_targ_entry = -1;
  instr_map[num_of_instr_map_entries].size = new_size;  
  instr_map[num_of_instr_map_entries].inlinedIndex = inlinedIndex;  
  instr_map[num_of_instr_map_entries].category_enum = XED_CATEGORY_UNCOND_BR;

  
  num_of_instr_map_entries++;

  // update expected size of tc:
  tc_cursor += new_size;         

  if (num_of_instr_map_entries >= max_ins_count) {
    cerr << "out of memory for map_instr" << endl;
    return -2;
  }


  //print the original and the new reverted cond instructions:
  //
  if(KnobDebug) cerr << "orig instr: " << hex << INS_Address(ins_tail) << " " << INS_Disassemble(ins_tail) << endl;

  char buf[2048];   
  xed_decoded_inst_t new_xedd;
  xed_decoded_inst_zero_set_mode(&new_xedd,&dstate);

  xed_error_enum_t xed_code = xed_decode(&new_xedd, reinterpret_cast<UINT8*>(instr_map[num_of_instr_map_entries-1].encoded_ins) , XED_MAX_INSTRUCTION_BYTES);
  if (xed_code != XED_ERROR_NONE) {
    cerr << "ERROR: xed decode failed for instr at: " << "0x" << hex << INS_Address(ins_tail) << endl;
    return -2;
  }

  xed_format_context(XED_SYNTAX_INTEL, &new_xedd, buf, 2048, INS_Address(ins_tail), 0, 0);
  if(KnobDebug) cerr << "new  instr: " << hex << INS_Address(ins_tail) << " " << buf << endl << endl;

  return new_size;
}


int find_candidate_rtns_for_translation(IMG img)
{
  int rc;
  int counter = 0;
  UINT64 inlinedIndex;
  RTN rtn;
  vector<RTNINFO*> rtnInfoVector;

  if(ifstream(PROFILE_FILE_NAME)) {
    updateProfile();
  }
  else {
    cerr << "Error: " << PROFILE_FILE_NAME << " doesn't exist" << endl;
    return -1;
  }

  for (RTNLIST::iterator rtnIt = rtnList.begin(); rtnIt != rtnList.end(); ++rtnIt)
    rtnInfoVector.push_back(rtnIt->second);

  sort(rtnInfoVector.begin(), rtnInfoVector.end(), compareFunc);


  for (vector<RTNINFO*>::iterator rtnIt = rtnInfoVector.begin(); rtnIt != rtnInfoVector.end(); ++rtnIt) {
    if(counter == OPT_RTN_NUM)
      break;
    else
      counter++;
    rtn = RTN_FindByAddress((*rtnIt)->addr);

    if (rtn == RTN_Invalid()) {
      cerr << "Warning: invalid routine " << RTN_Name(rtn) << endl;
      return -1;
    }

    translated_rtn[translated_rtn_num].rtn_addr = RTN_Address(rtn);     
    translated_rtn[translated_rtn_num].rtn_size = RTN_Size(rtn);
    translated_rtn[translated_rtn_num].instr_map_entry = num_of_instr_map_entries;
    translated_rtn[translated_rtn_num].isSafeForReplacedProbe = true; 

    // Ex3 Probing
    if(Opt1Knob.Value()) {
      // Open the RTN.
      RTN_Open( rtn );              

      for (INS ins = RTN_InsHead(rtn); INS_Valid(ins); ins = INS_Next(ins)) {

        //debug print of orig instruction:
        if (KnobVerbose) {
          cerr << "old instr: ";
          cerr << "0x" << hex << INS_Address(ins) << ": " << INS_Disassemble(ins) <<  endl;
        }       

        ADDRINT addr = INS_Address(ins);

        xed_decoded_inst_t xedd;
        xed_error_enum_t xed_code;              

        xed_decoded_inst_zero_set_mode(&xedd,&dstate); 

        xed_code = xed_decode(&xedd, reinterpret_cast<UINT8*>(addr), max_inst_len);
        if (xed_code != XED_ERROR_NONE) {
          cerr << "ERROR: xed decode failed for instr at: " << "0x" << hex << addr << endl;
          translated_rtn[translated_rtn_num].instr_map_entry = -1;
          break;
        }

        // Add instr into instr map:
        rc = add_new_instr_entry(&xedd, INS_Address(ins), 1, INS_Size(ins));
        if (rc < 0) {
          cerr << "ERROR: failed during instructon translation." << endl;
          translated_rtn[translated_rtn_num].instr_map_entry = -1;
          break;
        }

      } // end for INS...

      // Close the RTN.
      RTN_Close(rtn);     

      // debug print of routine name:
      if (KnobVerbose) {
        cerr <<   "rtn name: " << RTN_Name(rtn) << " : " << dec << translated_rtn_num << endl;
      }
    }
    // Ex4 Optimization
    else { /* if(Opt2Knob.Value()) */

      // BBLs order
      std::vector<BBLINFO> bblOptOrder = optimize_bbl_order((*rtnIt)->bblList, rtn);
      
      // Going through the new BBL order
      if(KnobDebug) cout << "AFTER REORDER" << endl;
      for (std::vector<BBLINFO>::iterator orderIt = bblOptOrder.begin(); orderIt!=bblOptOrder.end(); ++orderIt) {
        if(KnobDebug) {
          cout << hex << "! Start=0x" << (*orderIt).start << ", End=0x" << (*orderIt).end << 
                   ", FallThroughAddr=0x" << (*orderIt).fallThroughAddr << ", TakenAddr=0x" << (*orderIt).takenAddr << 
                   dec << ", FallThroughCount=" << (*orderIt).fallThroughCount << ", TakenCount=" << (*orderIt).takenCount << endl; 
        }

        std::vector<BBLINFO>::iterator orderItNext = orderIt+1;
        bool revertCondJump = false;
        bool addJump = false;
        bool removeJump = false;

        // The new order has switched the fallthrough and the taken 
        if(orderItNext!=bblOptOrder.end() && (*orderItNext).start != (*orderIt).fallThroughAddr && (*orderIt).fallThroughAddr!=0) {
          // Only need to revert the jump
          if((*orderItNext).start == (*orderIt).takenAddr) {
            if(KnobDebug) cout << "REVERT COND JUMP... maybe" << endl;
            revertCondJump = true;
          }
          else if ((*orderItNext).start == (*orderIt).takenAddr && (*orderIt).takenAddr != 0){
            if(KnobDebug) cout << "REMOVE JUMP... maybe" << endl;
            removeJump = true;
          }
          else {
            if(KnobDebug) cout << "ADD JUMP... maybe" << endl;
            addJump = true;
          }
        }
        RTN currentRtn = RTN_FindByAddress(orderIt->start);
        //RTN currentRtn = rtn;
        RTN_Open(currentRtn);
        if(currentRtn != rtn){
          inlinedIndex = ++inlinedIndexList[RTN_Address(currentRtn)];
          cout << "RTN: " << RTN_Name(currentRtn) << dec << " index: " << inlinedIndex << endl;
        }
        else{
          inlinedIndex = 0;
        }

        // Going through the all routing instructions to find the BBL location
        for (INS ins = RTN_InsHead(currentRtn); INS_Valid(ins); ins = INS_Next(ins)) {
          ADDRINT currAddr  = INS_Address(ins)/* - mainExeImageLowAddr*/;
          ADDRINT startAddr = (*orderIt).start;
          ADDRINT endAddr   = (*orderIt).end;
                 
          if(startAddr<=currAddr && currAddr<=endAddr) {
            //debug print of orig instruction:
            if (KnobVerbose) {
              cerr << "old instr: ";
              cerr << "0x" << hex << INS_Address(ins) << ": " << INS_Disassemble(ins) <<  endl;
            }       
        
            xed_decoded_inst_t xedd;
            xed_error_enum_t xed_code;              

            xed_decoded_inst_zero_set_mode(&xedd,&dstate); 

            xed_code = xed_decode(&xedd, reinterpret_cast<UINT8*>(currAddr), max_inst_len);
            if (xed_code != XED_ERROR_NONE) {
              cerr << "ERROR: xed decode failed for instr at: " << "0x" << hex << currAddr << endl;
              translated_rtn[translated_rtn_num].instr_map_entry = -1;
              break;
            }

            if(rtn != currentRtn) { // This is an inlined function and it is its last instruction (assuming it is ret) 
              xed_decoded_inst_t *xedd2 = INS_XedDec(ins);
              
              xed_category_enum_t category_enum = xed_decoded_inst_get_category(xedd2);

              if (category_enum == XED_CATEGORY_RET){
                cout << "Removing RET @ 0x" << hex << INS_Address(ins) << ": " << INS_Disassemble(ins) <<  endl;
                add_new_add_instr_entry(inlinedIndex);
                if(orderItNext!=bblOptOrder.end() && (*orderItNext).start != (*orderIt).fallThroughAddr && (*orderIt).fallThroughAddr!=0) {
                  cout << "Adding jmp to 0x" << (*orderIt).fallThroughAddr << endl;
                  add_new_jmp_instr_entry((*orderIt).fallThroughAddr, true);
                }
                continue;
              }
            } 
            // Add instr into instr map:
            if(currAddr==endAddr  && revertCondJump!=0) {
              if(KnobDebug) cout << "REVERT COND" << endl;
              rc = revert_cond_jump(ins, (*orderIt).fallThroughAddr, inlinedIndex);
              if(rc == -1) {
                cerr << "WARNING: conditional jump was not reverted, inserting normal instruction" << endl;
                rc = add_new_instr_entry(&xedd, INS_Address(ins), inlinedIndex, INS_Size(ins));

              }
              if(rc == 3) { // Call instruction
                cout << "0x" << hex << INS_Address(ins) << ": " << INS_Disassemble(ins) <<  endl;
					      add_new_sub_instr_entry(inlinedIndex);
					      continue;
              }
            }
            else if(currAddr==endAddr && addJump==true) {
              
              rc = add_new_instr_entry(&xedd, INS_Address(ins), inlinedIndex, INS_Size(ins));
            
              // If the jumps is unconditional then we do not need to add another jump afterwards 
              xed_decoded_inst_t *xedd = INS_XedDec(ins);
              xed_category_enum_t category_enum = xed_decoded_inst_get_category(xedd);
              if (category_enum != XED_CATEGORY_UNCOND_BR) {
                if(KnobDebug) cout << "ADDING JUMP" << endl;
                rc = add_new_jmp_instr_entry((*orderIt).fallThroughAddr, inlinedIndex);
              }
            }
            else if (currAddr == endAddr && removeJump){
              xed_decoded_inst_t *xedd_tmp = INS_XedDec(ins);
              xed_category_enum_t category_enum = xed_decoded_inst_get_category(xedd_tmp);
              if (category_enum != XED_CATEGORY_UNCOND_BR) {
                rc = add_new_instr_entry(&xedd, INS_Address(ins), inlinedIndex, INS_Size(ins));
              }
              else{
                rc = 0;
              }
            }
            else {
              rc = add_new_instr_entry(&xedd, INS_Address(ins), inlinedIndex, INS_Size(ins));
            }
            if (rc < 0) {
              cerr << "ERROR: failed during instructon translation." << endl;
              translated_rtn[translated_rtn_num].instr_map_entry = -1;
              break;
            }
          }
        } 
        RTN_Close(currentRtn);
      }         
      
      // debug print of routine name:
      if (KnobVerbose) {
        cerr <<   "rtn name: " << RTN_Name(rtn) << " : " << dec << translated_rtn_num << endl;
      }     
    }
      
    //exit(0);

    translated_rtn_num++;
  }
  return 0;
}

int copy_instrs_to_tc()
{
  int cursor = 0;

  for (int i=0; i < num_of_instr_map_entries; i++) {

    if ((ADDRINT)&tc[cursor] != instr_map[i].new_ins_addr) {
      cerr << "ERROR: Non-matching instruction addresses: " << hex << (ADDRINT)&tc[cursor] << " vs. " << instr_map[i].new_ins_addr << endl;
      return -1;
    }   


    memcpy(&tc[cursor], &instr_map[i].encoded_ins, instr_map[i].size);

    cursor += instr_map[i].size;
  }

  return 0;
}

inline void commit_translated_routines() 
{
  // Commit the translated functions: 
  // Go over the candidate functions and replace the original ones by their new successfully translated ones:

  for (int i=0; i < translated_rtn_num; i++) {

    //replace function by new function in tc

    if (translated_rtn[i].instr_map_entry >= 0) {

      if (translated_rtn[i].rtn_size > MAX_PROBE_JUMP_INSTR_BYTES && translated_rtn[i].isSafeForReplacedProbe) {            

        RTN rtn = RTN_FindByAddress(translated_rtn[i].rtn_addr);

        //debug print:        
        if (rtn == RTN_Invalid()) {
          cerr << "committing rtN: Unknown";
        } else {
          cerr << "committing rtN: " << RTN_Name(rtn);
        }
        cerr << " from: 0x" << hex << RTN_Address(rtn) << " to: 0x" << hex << instr_map[translated_rtn[i].instr_map_entry].new_ins_addr << endl;


        if (RTN_IsSafeForProbedReplacement(rtn)) {

          AFUNPTR origFptr = RTN_ReplaceProbed(rtn,  (AFUNPTR)instr_map[translated_rtn[i].instr_map_entry].new_ins_addr);             

          if (origFptr == NULL) {
            cerr << "RTN_ReplaceProbed failed.";
          } else {
            cerr << "RTN_ReplaceProbed succeeded. ";
          }
          cerr << " orig routine addr: 0x" << hex << translated_rtn[i].rtn_addr
            << " replacement routine addr: 0x" << hex << instr_map[translated_rtn[i].instr_map_entry].new_ins_addr << endl; 

          dump_instr_from_mem ((ADDRINT *)translated_rtn[i].rtn_addr, translated_rtn[i].rtn_addr);                        
        }                       
      }
    }
  }
}

int allocate_and_init_memory(IMG img) 
{
  // Calculate size of executable sections and allocate required memory:
  //
  for (SEC sec = IMG_SecHead(img); SEC_Valid(sec); sec = SEC_Next(sec))
  {   
    if (!SEC_IsExecutable(sec) || SEC_IsWriteable(sec) || !SEC_Address(sec))
      continue;


    if (!lowest_sec_addr || lowest_sec_addr > SEC_Address(sec))
      lowest_sec_addr = SEC_Address(sec);

    if (highest_sec_addr < SEC_Address(sec) + SEC_Size(sec))
      highest_sec_addr = SEC_Address(sec) + SEC_Size(sec);

    // need to avouid using RTN_Open as it is expensive...
    for (RTN rtn = SEC_RtnHead(sec); RTN_Valid(rtn); rtn = RTN_Next(rtn))
    {   

      if (rtn == RTN_Invalid())
        continue;

      max_ins_count += RTN_NumIns  (rtn);
      max_rtn_count++;
    }
  }

  max_ins_count *= 4; // estimating that the num of instrs of the inlined functions will not exceed the total nunmber of the entire code.

  // Allocate memory for the instr map needed to fix all branch targets in translated routines:
  instr_map = (instr_map_t *)calloc(max_ins_count, sizeof(instr_map_t));
  if (instr_map == NULL) {
    perror("calloc");
    return -1;
  }


  // Allocate memory for the array of candidate routines containing inlineable function calls:
  // Need to estimate size of inlined routines.. ???
  translated_rtn = (translated_rtn_t *)calloc(max_rtn_count, sizeof(translated_rtn_t));
  if (translated_rtn == NULL) {
    perror("calloc");
    return -1;
  }


  // get a page size in the system:
  int pagesize = sysconf(_SC_PAGE_SIZE);
  if (pagesize == -1) {
    perror("sysconf");
    return -1;
  }

  ADDRINT text_size = (highest_sec_addr - lowest_sec_addr) * 2 + pagesize * 4;

  int tclen = 2 * text_size + pagesize * 4;   // need a better estimate???

  // Allocate the needed tc with RW+EXEC permissions and is not located in an address that is more than 32bits afar:    
  char * addr = (char *) mmap(NULL, tclen, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
  if ((ADDRINT) addr == 0xffffffffffffffff) {
    cerr << "failed to allocate tc" << endl;
    return -1;
  }

  tc = (char *)addr;
  return 0;
}

VOID ImageLoad(IMG img, VOID *v)
{
  // debug print of all images' instructions
  //dump_all_image_instrs(img);


  // Step 0: Check the image and the CPU:
  if (!IMG_IsMainExecutable(img))
    return;

  int rc = 0;

  mainExeImageLowAddr = IMG_LowAddress(img);
  mainExeImageHighAddr = IMG_HighAddress(img);    

  // step 1: Check size of executable sections and allocate required memory:  
  rc = allocate_and_init_memory(img);
  if (rc < 0)
    return;

  cout << "after memory allocation" << endl;

  // Step 2: go over the profiled routines and copy OPT_RTN_NUM of their code into the instr map IR:
  rc = find_candidate_rtns_for_translation(img);
  if (rc < 0)
    return;

  cout << "after identifying candidate routines" << endl;  

  // Step 3: Chaining - calculate direct branch and call instructions to point to corresponding target instr entries:
  rc = chain_all_direct_br_and_call_target_entries();
  if (rc < 0 )
    return;

  cout << "after calculate direct br targets" << endl;

  // Step 4: fix rip-based, direct branch and direct call displacements:
  rc = fix_instructions_displacements();
  if (rc < 0 )
    return;

  cout << "after fix instructions displacements" << endl;


  // Step 5: write translated routines to new tc:
  rc = copy_instrs_to_tc();
  if (rc < 0 )
    return;

  cout << "after write all new instructions to memory tc" << endl;

  if (KnobDumpTranslatedCode) {
    cerr << "Translation Cache dump:" << endl;
    dump_tc();  // dump the entire tc

    cerr << endl << "instructions map dump:" << endl;
    dump_entire_instr_map();     // dump all translated instructions in map_instr
  }



  if(KnobDoNotCommit.Value()==0) {
	  // Step 6: Commit the translated routines:
	  //Go over the candidate functions and replace the original ones by their new successfully translated ones:
	  commit_translated_routines(); 
	  //exit(0);
	  cout << "after commit translated routines" << endl;
  }
  else {
	  cout << "Do not commit knob toggeled" << endl;
  }

}



INT32 Usage()
{
  cerr << "This tool translated routines of an Intel(R) 64 binary"
    << endl;
  cerr << KNOB_BASE::StringKnobSummary();
  cerr << endl;
  return -1;
}


int main(int argc, char * argv[])
{
  // Initialize symbol table code, needed for rtn instrumentation
  PIN_InitSymbols();

  // Initialize pin
  if (PIN_Init(argc, argv)) return Usage();

  // Ex2 Edge Profiling
  if(ProfKnob.Value()) {
    outFile.open("rtn-output.txt");

    IMG_AddInstrumentFunction(Image, 0);
    TRACE_AddInstrumentFunction(Trace, 0);
    PIN_AddFiniFunction(Fini, 0);

    PIN_StartProgram();
  }

  // Ex3/4 Probe Mode
  if(Opt1Knob.Value() || Opt2Knob.Value() || Opt3Knob.Value()) {
    IMG_AddInstrumentFunction(ImageLoad, 0); 

    PIN_StartProgramProbed();
  }

  return 0;
}
