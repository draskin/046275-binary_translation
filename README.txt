046275 - Dynamic Binary Translation and Optimization Techniques
===============================================================
EX4 & FINAL PROJECT

Gil Shomron		301722294	gilsho@tx.technion.ac.il
Daniel Raskin	305386419	raskindaniel119@gmail.com

PAPER
=====
Please find REPORT.pdf with all the information about the results and implementation.

COMPILATION
==========
Use template in one of the <PIN_ROOT>/source/tools/* directories to compile our Pin tool using `make obj-intel64/project.so`.

Another way:
1. Copy project.cpp and the makefile and makefile.rules (*) files into <PIN_ROOT>/source/tools/MyPinTool directory
2. make
3. pin -t obj-intel64/project.so <FLAGS> -- <DUT> 

(*) makefiles are based on MyPinTool directory templates
